﻿using Microsoft.Kinect;
using System;
using System.Linq;
using System.Windows.Controls;
using System.Drawing;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.IO;
using System.Drawing.Imaging;
using System.Drawing.Drawing2D;

namespace FaceTrackingBasics {

    public class Main {

        static MainWindow main_window;
        static int counter = 0;
        static bool person_found = false;
        static List<Color> colors = new List<Color>();
        static Color found_color;
        static Color chosen_color;
        static ColorImageFrame color_frame = null;
        static WriteableBitmap writeable_bitmap = null;
        static TextBlock text_block = null;
        static TextBlock text_block_found = null;

        public static void Log(string log) { Console.WriteLine(log); }

        public static void SetColorFrame(AllFramesReadyEventArgs e) { color_frame = e.OpenColorImageFrame(); }

        public static void SetWriteableBitmap(WriteableBitmap wb) { writeable_bitmap = wb; }

        public static WriteableBitmap GetWriteableBitmap() { return writeable_bitmap; }

        public static void Initialise(MainWindow window, TextBlock block, TextBlock found_block) {
            Log("Starting App...");
            main_window = window;
            text_block = block;
            text_block_found = found_block;

            Log("Init Colors");
            colors.Add(Color.Red);
            colors.Add(Color.Orange);
            colors.Add(Color.Yellow);
            colors.Add(Color.Green);
            colors.Add(Color.Blue);
            colors.Add(Color.Purple);
            colors.Add(Color.Black);
            colors.Add(Color.Gray);
        }

        public static void OnPersonFound() {
            if (person_found) {
                text_block.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(chosen_color.A, chosen_color.R, chosen_color.G, chosen_color.B));
                text_block_found.Background = new System.Windows.Media.SolidColorBrush(System.Windows.Media.Color.FromArgb(found_color.A, found_color.R, found_color.G, found_color.B));
                Color color = chosen_color;
                Music.Colors the_color;
                if (color == Color.Red) {
                    the_color = Music.Colors.Red;
                } else if (color == Color.Orange) {
                    the_color = Music.Colors.Orange;
                } else if (color == Color.Yellow) {
                    the_color = Music.Colors.Yellow;
                } else if (color == Color.Green) {
                    the_color = Music.Colors.Green;
                } else if (color == Color.DarkBlue) {
                    the_color = Music.Colors.Blue;
                } else if (color == Color.Purple) {
                    the_color = Music.Colors.Purple;
                } else if (color == Color.Black) {
                    the_color = Music.Colors.Black;
                } else if (color == Color.Gray) {
                    the_color = Music.Colors.Grey;
                } else {
                    the_color = Music.Colors.Red;
                }

                new Music(the_color);
            }
        }

        public static void SetTracked() {
            person_found = true;
            if (counter > 400) {
                counter = 0;
                Main.OnPersonFound();
            }
            Main.counter++;
            Log(counter.ToString());
        }

        internal static void Dispose() {
            if (color_frame != null) { color_frame.Dispose(); }
        }

        public static void OnTracked() {
            SetTracked();
            Color color = GetRGBColor(writeable_bitmap);
            found_color = color;
            string hex = color.R.ToString("X2") + color.G.ToString("X2") + color.B.ToString("X2");
            chosen_color = GetNearestColorLabelIndex(color, colors);
            Main.Dispose();
        }
        
        private static Color GetRGBColor(WriteableBitmap image) {
            Bitmap bitmap = BitmapFromWriteableBitmap(image);
            int X = Convert.ToInt32(image.Width) / 2;
            int Y = Convert.ToInt32(image.Height) / 2;

            Color color = bitmap.GetPixel(X, Y);
            bitmap.Dispose();
            return color;
        }

        private static System.Drawing.Bitmap BitmapFromWriteableBitmap(WriteableBitmap writeBmp) {
            System.Drawing.Bitmap bmp;
            using (MemoryStream outStream = new MemoryStream()) {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create((BitmapSource)writeBmp));
                enc.Save(outStream);
                bmp = new System.Drawing.Bitmap(outStream);
            }
            return bmp;
        }

        public static Color GetNearestColorLabelIndex(Color chosen_color, List<Color> LabelColorsHSB) {
            double dbl_input_red = Convert.ToDouble(chosen_color.R);
            double dbl_input_green = Convert.ToDouble(chosen_color.G);
            double dbl_input_blue = Convert.ToDouble(chosen_color.B);
            double dbl_test_red, dbl_test_green, dbl_test_blue;
            double distance = 500.0;

            Color nearest_color = Color.Empty;
            foreach (Color color in LabelColorsHSB) {
                dbl_test_red = Math.Pow(Convert.ToDouble(color.R) - dbl_input_red, 2.0);
                dbl_test_green = Math.Pow(Convert.ToDouble(color.G) - dbl_input_green, 2.0);
                dbl_test_blue = Math.Pow(Convert.ToDouble(color.B) - dbl_input_blue, 2.0);

                double temp = Math.Sqrt(dbl_test_blue + dbl_test_green + dbl_test_red);
                if (temp == 0.0) {
                    nearest_color = color;
                    break;
                } else if (temp < distance) {
                    distance = temp;
                    nearest_color = color;
                }
            }
            return nearest_color;
        }
    }
}
