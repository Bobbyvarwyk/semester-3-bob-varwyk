﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1
{
    class MusicPlayer
    {


        public enum Colors
        {
            red,
            orange,
            purple,
            blue,
            green,
            yellow,
            black,
            brown
        }


        public string selectSong(Colors colors)
        {

            string song = "";

            switch(colors)
            {
                default:
                case Colors.red:
                    song = "Isolation.wav";
                    break;
                case Colors.orange:
                    song = "https://open.spotify.com/track/2GyH5rvdnfkjzsTFaWrrov?si=H5rOtZFdQZ-T1SIKCCdbBQ";
                    break;
                case Colors.purple:
                    song = "https://open.spotify.com/track/7Cuk8jsPPoNYQWXK9XRFvG?si=NnJdePpsTCKZ9ve6pJDHdQ";
                    break;
                case Colors.blue:
                    song = "https://open.spotify.com/track/6gRGLfswEsfpy1UxHEyP6X?si=blaar36OSA-9OvR52SttcA";
                    break;
                case Colors.green:
                    song = "https://open.spotify.com/track/0n4bITAu0Y0nigrz3MFJMb?si=Qcm7KDufSkeQZrluKH1zvg";
                    break;
                case Colors.yellow:
                    song = "https://open.spotify.com/track/5oX3nujmD03IvRuMuhdYBl?si=8YBguwRJSiKfFRD7BlKaPg";
                    break;
                case Colors.black:
                    song = "https://open.spotify.com/track/4m88jdHYNRCU7vtdjfp0fO?si=v13_vGpQQD2-V15RkDss8g";
                    break;
            }
            return song;
        }


    }
}
