﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaceTrackingBasics {

    class Music {

        private Colors current_color = Colors.Pink;

        public enum Colors { Red, Orange, Purple, Blue, Green, Yellow, Black, Brown, Pink, Grey }

        public Music(Colors the_color) {
            if (current_color != the_color) {
                System.Diagnostics.Process.Start(SelectSong(the_color));
            }
        }

        public string SelectSong(Colors colors) {
            string song = "";
            switch (colors) {
                case Colors.Red:
                    current_color = Colors.Red;
                    song = "https://open.spotify.com/track/5G2c0FDSToVnmhrk9EJuv2";
                    break;
                case Colors.Orange:
                    current_color = Colors.Orange;
                    song = "https://open.spotify.com/track/2GyH5rvdnfkjzsTFaWrrov";
                    break;
                case Colors.Yellow:
                    current_color = Colors.Yellow;
                    song = "https://open.spotify.com/track/5oX3nujmD03IvRuMuhdYBl";
                    break;
                case Colors.Green:
                    current_color = Colors.Green;
                    song = "https://open.spotify.com/track/0n4bITAu0Y0nigrz3MFJMb";
                    break;
                case Colors.Blue:
                    current_color = Colors.Blue;
                    song = "https://open.spotify.com/track/6gRGLfswEsfpy1UxHEyP6X";
                    break;
                case Colors.Purple:
                    current_color = Colors.Purple;
                    song = "https://open.spotify.com/track/7Cuk8jsPPoNYQWXK9XRFvG";
                    break;
                case Colors.Grey:
                    current_color = Colors.Grey;
                    song = "https://open.spotify.com/track/7Fg4jpwpkdkGCvq1rrXnvx";
                    break;
                case Colors.Black:
                    current_color = Colors.Black;
                    song = "https://open.spotify.com/track/4m88jdHYNRCU7vtdjfp0fO";
                    break;
                default:
                    current_color = Colors.Pink;
                    song = "https://www.webdesignerdepot.com/cdn-origin/uploads/2017/05/featured_404.jpg";
                    break;
            }
            song = song.Replace("track", "track");
            return string.Format("{0}", song);
        }
    }
}
