<!DOCTYPE HTML>
<html>
<head>
    <title> Usa Unemploymeny Rate 2000 - 2010  </title>
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Barlow:700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="styles.css" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
    <div class="container"> 
        <div class="title-bar"> 
            <h1> Unemployment statistics USA 2007 - 2010</h1>
        </div>

        <div class="stats-grid">

            <?php 
                $id = 66;

                for($year = 2007; $year <= 2010; $year++) {
                    $id++;
            ?>
                    <canvas class="styler" data-year="<?php echo $id; ?>" id="<?php echo $year; ?>" width="350" height="350"></canvas>
            <?php
                }
            ?>
        </div>
    </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.2/Chart.bundle.min.js"> </script>

<script type="text/javascript">
 	$(document).ready(function () {
    $.ajax({
                url: "json/jobmarket.json",
                dataType: "json",
                success: showData
            }
    );
    
    function showData(data) {

        for(year = 2007; year <= 2010; year++) {
            var year_id = document.getElementById(year);
            var id = $(year_id).attr("data-year");
            console.log(year);
            console.log(id);

            new Chart(document.getElementById(year), {
                type: 'doughnut',
                data: {
                labels: ["Unemployed", "Employed", "Not active"],
                datasets: [
                    {
                    label: "Employment statistics" + data.jobmarket[id].year,
                    backgroundColor: ["#C0392B", "#4DAF7C","#6BB9F0"],
                    data: [data.jobmarket[id].unemployed, data.jobmarket[id].employed_total, data.jobmarket[id].not_in_labor]
                    }
                ]
                },
                options: {
                    responsive:false,
                    maintainAspectRatio: false,
                title: {
                    display: true,
                    text: "Employment statistics " + data.jobmarket[id].year
                }
                }
            });  
        }
    }
});
</script>
</body>
</html>

