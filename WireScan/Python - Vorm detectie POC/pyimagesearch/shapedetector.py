import cv2

class ShapeDetector:
    def __init__(self):
        pass

    def detect(self, c):
        shape = "unidentified"
        peri = cv2.arcLength(c, True)
        corners = cv2.approxPolyDP(c, 0.04 * peri, True)

        if len(corners) == 3:
            shape = "triangle"

        elif len(corners) == 4:
            (x, y, w, h) = cv2.boundingRect(corners)
            ar = w / float(h)
            
            shape = "square" if ar >= 0.95 and ar <= 1.05 else "rectangle"

        elif len(corners) == 5:
            shape = "pentagon"

        else:
            shape = "circle"

        return shape