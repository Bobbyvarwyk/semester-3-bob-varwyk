<html>
<head>
	<meta charset="Utf-8"> 
	<link rel="stylesheet" type="text/css" href="styles.css" />
	<link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
</head>

<body>
	<div class="dashboard">
		<div class="left">
			<div id="video"></div>
		</div>
		<div class="right">
			<div class="inner-content">

				<div class="controls">
					<h1> Wireframe to HTML </h1>
					<p> Place your wireframe under the camera, click on the button to scan your wireframe. </p>
					<button onclick="showCog()" class="scanner" type="submit" name="submit"> Scan your wireframe </button>
					
				</div>

				<i class="fas fa-cog" id="cog"></i>

				<div class="finished" id="finish">
					<h1> HTML Generated </h1>
					<p> Once your HTML file is finished, you can download it by clicking the button below. </p>
					<a href="wireframe.zip" class="download"> Download HTML layout </a>
				</div>

			</div>
		</div>
	</div>
</body>

<?php 

	function setBridge() {
		$string = 'cd C:\xampp\htdocs\wireframe-scanner\python';
		$output = shell_exec($string);
		echo $output;
	}

	setBridge();

	function execute() {
		$string = 'python test.py';
		$output = shell_exec($string);
		var_dump($output);
	}
?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="js/main.js"></script>
</html>
