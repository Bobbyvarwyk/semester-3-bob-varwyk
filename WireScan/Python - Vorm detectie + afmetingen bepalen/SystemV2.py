from scipy.spatial import distance as dist
from pyimagesearch.shapedetector import ShapeDetector
from imutils import perspective
from imutils import contours
import numpy as np
from PIL import Image
import argparse
import imutils
import cv2

# Definier het middelste punt om de objecten juist te kunnen meten.
def midpoint(startPoint, endPoint):
    return ((startPoint[0] + endPoint[0]) * 0.5, (startPoint[1] + endPoint[1]) * 0.5)

# Zet de argumentparser voor de command line op. zowel het path van de afbeelding als de breedte van het meest linker object. 
ap = argparse.ArgumentParser()
ap.add_argument("-i", "--image", required=True, help="Path to image")
ap.add_argument("-w", "--width", required=True, help="Width of the most left item in the image")
args = vars(ap.parse_args())

image = cv2.imread(args['image'])
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
gray = gray = cv2.GaussianBlur(gray, (7, 7), 0)

# Detecteer de randen
edged = cv2.Canny(gray, 50, 100)
edged = cv2.dilate(edged, None, iterations=1)
edged = cv2.erode(edged, None, iterations=1)

# Vind de contouren in de afbeelding

cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cnts = imutils.grab_contours(cnts)
(cnts, _) = contours.sort_contours(cnts) 

htmlList = []
scanner = ShapeDetector()


def scanShapes():
     
    pixelsPerMetric = None

    for c in cnts:

        shape = scanner.detect(c)
        
        if cv2.contourArea(c) < 100 :
            continue

        # Get Box boundings
        
        box = cv2.minAreaRect(c)
        box = cv2.cv.BoxPoints(box) if imutils.is_cv2() else cv2.boxPoints(box) #!!!!!
        box = np.array(box, dtype = "int")

        box = perspective.order_points(box)
        cv2.drawContours(image, [box.astype("int")], -1, (0, 255, 0), 2)

        for (x, y) in box:
            cv2.circle(image, (int(x), int(y)), 5, (0, 0, 255), -1)

        # loop door alle punten en teken ze

        (tl, tr, br, bl) = box
        (tltrX, tltrY) = midpoint(tl, tr)
        (blbrX, blbrY) = midpoint(bl, br)

        (tlblX,tlblY) = midpoint(tl, bl)
        (trbrX,trbrY) = midpoint(tr, br)

        dA = dist.euclidean((tltrX, tltrY), (blbrX, blbrY))
        dB = dist.euclidean((tlblX, tlblY), (trbrX, trbrY))

        if pixelsPerMetric is None:
            pixelsPerMetric = dB / float(args["width"])

        dimA = dA / pixelsPerMetric
        dimB = dB / pixelsPerMetric

        cv2.putText(image, "{:.1f}in".format(dimB), (int(tltrX - 15), int(tltrY -10)), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (255, 255, 255), 2)
        cv2.putText(image, "{:.1f}in".format(dimA), (int(trbrX + 10), int(trbrY)), cv2.FONT_HERSHEY_SIMPLEX, 0.65, (255, 255, 255), 2)

        pixelsA = convertToPixels(dimA)
        pixelsB = convertToPixels(dimB)

        divString = [shape, "{:.1f}".format(pixelsA), "{:.1f}".format(pixelsB)]
        htmlList.append(divString)

    print(htmlList) 

cv2.imshow("Image", image)
cv2.waitKey(0) 

def convertToPixels(Inches):

    # DPI van de PNG is 300, dus de inches keer 300 is het aantal gewenste pixels.
    pixels = Inches * 300

    return pixels

scanShapes()


