﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Net;

namespace WindowsFormsApp1
{
    public partial class JsonParser
    {
        [JsonProperty("xy")]
        public float[] xy { get; set; }
        [JsonProperty("transitiontime")]
        public int transitiontime { get; set; }

        public JsonParser(float[] xy_code, int transition)
        {
            xy = xy_code;
            transitiontime = transition;
        }
    }
}
